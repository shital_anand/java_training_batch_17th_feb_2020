package com.example.EmployeeManagementAPI.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Getter
@Setter
public @Data
class Employee extends Auditable<String> {
    @Id
    private int empId;
    private String empName;
    private String email;
    private boolean active;

    public Employee() {

    }

    public Employee(int empId, String empName, String email, boolean active) {
        this.empId = empId;
        this.empName = empName;
        this.email = email;
        this.active = active;
    }

    public Employee(int empId, String empName, String email, boolean active, List<Department> department) {
        this.empId = empId;
        this.empName = empName;
        this.email = email;
        this.active = active;
        this.department = department;
    }

    @ManyToMany
    private List<Department> department;

    public Employee(int i, List<Department> departmentList) {
        this.empId = i;
        this.department = departmentList;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
