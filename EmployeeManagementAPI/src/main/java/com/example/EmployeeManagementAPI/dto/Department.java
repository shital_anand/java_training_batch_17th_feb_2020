package com.example.EmployeeManagementAPI.dto;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity

public @Data
class Department extends Auditable<String>{
    @Id
    private int departId;
    private String departName;

    public Department(){

    }
    public Department(int departId, String departName, List<Employee> employees) {
        this.departId = departId;
        this.departName = departName;
        this.employees = employees;
    }

    public Department(int departId, String departName) {
        this.departId = departId;
        this.departName = departName;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Employee> employees;

    public Department(int departId) {
        this.departId=departId;
    }
}
