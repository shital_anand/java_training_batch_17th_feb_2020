package com.example.EmployeeManagementAPI.dto;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class SpringSecurityAuditorAware implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable("Admin").filter(s -> !s.isEmpty());
    }
}
