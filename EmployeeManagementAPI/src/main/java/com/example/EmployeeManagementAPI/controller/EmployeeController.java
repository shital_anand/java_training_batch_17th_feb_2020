package com.example.EmployeeManagementAPI.controller;

import com.example.EmployeeManagementAPI.dto.Employee;
import com.example.EmployeeManagementAPI.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {
    @Autowired
    private IEmployeeService empServiceRef;

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return empServiceRef.getAllEmployees();
    }

    @RequestMapping("/employees/{empid}")
    public Optional<Employee> getEmployeeById(@PathVariable int empid) {
        return empServiceRef.getEmployeeById(empid);
    }

    @DeleteMapping("/employees/{empId}")
    public String deleteEmployeeById(@PathVariable int empId) {

        empServiceRef.deleteEmployee(empId);
        return "Employee with employee id: "+empId+" is deactived or removed from an organization.";
    }

    @PostMapping("/employees/{departId}")
    public String addEmployee(@RequestBody Employee employee, @PathVariable int departId) {

        empServiceRef.addEmployee(employee, departId);
        return "Employee is added successfully.";
    }

    @PostMapping("/employees/{empId}/{departId}")
    public String addAnotherDepartment(@PathVariable int empId, @PathVariable int departId) {

        empServiceRef.addAnotherDepartment(empId, departId);
        return "The employee with employee id: "+empId+" is successfully added to department id: "+departId+".";

    }

    @PutMapping("/employees")
    public String updateEmployee(@RequestBody Employee employee) {
        empServiceRef.updateEmployee(employee);
        return "Employee is updated successfully.";
    }
}
