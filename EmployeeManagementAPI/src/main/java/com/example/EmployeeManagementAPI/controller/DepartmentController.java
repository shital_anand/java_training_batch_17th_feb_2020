package com.example.EmployeeManagementAPI.controller;

import com.example.EmployeeManagementAPI.dto.Department;
import com.example.EmployeeManagementAPI.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {
    @Autowired
    IDepartmentService departmentService;

    @RequestMapping("/departments")
    public List<Department> getAllDepartments() {
        return departmentService.getAllDepartments();
    }

    @PostMapping("/employees/departments")
    public String addDepartment(@RequestBody Department department) {
        departmentService.addDepartment(department);
        return "New Department is successfully added.";
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/departments/{departId}")
    public String updateDepartment(@RequestBody Department department, @PathVariable int departId) {
        departmentService.updateDepartment(department, departId);
        return "Department with department id: "+departId+" is successfully updated.";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/departments/{departId}")
    public String deleteDepartmentById(@PathVariable int departId) {
        departmentService.deleteDepartmentById(departId);
        return "Department with department id: "+departId +" is successfully removed from an organization.";
    }
}
