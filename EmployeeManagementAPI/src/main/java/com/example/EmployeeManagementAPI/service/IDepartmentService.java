package com.example.EmployeeManagementAPI.service;

import com.example.EmployeeManagementAPI.dto.Department;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IDepartmentService {

    List<Department> getAllDepartments();

    List<Department> getDepartmentList(int empId);

    Optional<Department> geDepartmentById(int departId);

    void addDepartment(Department department);

    void updateDepartment(Department department, int departId);

    void deleteDepartmentById(int departId);

}
