package com.example.EmployeeManagementAPI.service;

import com.example.EmployeeManagementAPI.dto.Employee;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface IEmployeeService {

    List<Employee> getAllEmployees();

    Optional<Employee> getEmployeeById(int empId);

    void addEmployee(Employee employee, int departId);

    void addAnotherDepartment(int empId, int departId);

    void deleteEmployee(int empId);

    void updateEmployee(Employee employee);
}
