package com.example.EmployeeManagementAPI.service;

import com.example.EmployeeManagementAPI.dto.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {

}
