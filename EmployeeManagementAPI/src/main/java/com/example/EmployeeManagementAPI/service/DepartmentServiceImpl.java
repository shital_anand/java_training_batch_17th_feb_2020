package com.example.EmployeeManagementAPI.service;

import com.example.EmployeeManagementAPI.dto.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class DepartmentServiceImpl implements IDepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;


    @Override
    public List<Department> getAllDepartments() {
        List<Department> departments = new ArrayList<>();
        departmentRepository.findAll().forEach(departments::add);
        return departments;
    }

    @Override
    public List<Department> getDepartmentList(int empId) {
        return null;
    }


    @Override
    public Optional<Department> geDepartmentById(int departId) {
        return departmentRepository.findById(departId);
    }


    @Override
    public void addDepartment(Department department) {
        departmentRepository.save(department);

    }

    @Override
    public void updateDepartment(Department department, int departId) {
        departmentRepository.save(department);

    }

    @Override
    public void deleteDepartmentById(int departId) {
        departmentRepository.deleteById(departId);

    }


}
