package com.example.EmployeeManagementAPI.service;

import com.example.EmployeeManagementAPI.dto.Department;
import com.example.EmployeeManagementAPI.dto.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class EmployeeServiceImpl implements IEmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    private DepartmentRepository departmentRepository;


    @Override
    public List<Employee> getAllEmployees() {
        return (List<Employee>) employeeRepository.findAll();
    }

    public Optional<Employee> getEmployeeById(int empId) {

        return employeeRepository.findById(empId);

    }

    @Override
    public void addEmployee(Employee employee, int departId) {
        Department department = new Department(departId, "");
        List<Department> departmentList = new ArrayList<>();
        departmentList.add(department);

        Employee employee1 = new Employee(employee.getEmpId(), departmentList);
        employee1.setActive(true);
        employee1.setEmpName(employee.getEmpName());
        employee1.setEmail(employee.getEmail());
        employeeRepository.save(employee1);

    }

    @Override
    public void addAnotherDepartment(int empId, int departId) {
        employeeRepository.addDepartment(empId, departId);
    }

    @Override
    public void deleteEmployee(int empId) {
        Employee employee1 = employeeRepository.findById(empId).orElseThrow();
        employee1.setActive(false);
        employeeRepository.save(employee1);


    }

    @Override
    public void updateEmployee(Employee employee) {
        int empId = employee.getEmpId();
        Employee employee1 = employeeRepository.findById(empId).orElseThrow();
        employee1.setEmail(employee.getEmail());

        employeeRepository.save(employee1);
    }


}
