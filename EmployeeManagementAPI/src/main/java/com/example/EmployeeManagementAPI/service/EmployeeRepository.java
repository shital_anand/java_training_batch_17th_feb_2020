package com.example.EmployeeManagementAPI.service;

import com.example.EmployeeManagementAPI.dto.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Modifying
    @Transactional
    @Query(value = "INSERT INTO EMPLOYEE_DEPARTMENT(EMPLOYEE_EMP_ID,DEPARTMENT_DEPART_ID ) VALUES(:empId,:departId)", nativeQuery = true)
    void addDepartment(int empId, int departId);

}
