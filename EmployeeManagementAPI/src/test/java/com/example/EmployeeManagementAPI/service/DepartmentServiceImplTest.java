package com.example.EmployeeManagementAPI.service;

import com.example.EmployeeManagementAPI.dto.Department;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

class DepartmentServiceImplTest extends Object {
    @InjectMocks
    DepartmentServiceImpl departmentService;
    @Mock
    DepartmentRepository departmentRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllDepartments() {
        Department department = new Department();
        Department department1 = new Department();
        List<Department> departmentList = new ArrayList<>();

        department.setDepartId(1);
        department.setDepartName("HR");

        department1.setDepartId(2);
        department1.setDepartName("Manager");

        departmentList.add(department);
        departmentList.add(department1);
        when(departmentRepository.findAll()).thenReturn(departmentList);
        assertNotNull(department);
    }

    @Test
    void geDepartmentById() {
        Department department = new Department();
        department.setDepartId(1);
        when(departmentRepository.findById(department.getDepartId())).thenReturn(java.util.Optional.of(department));

        assertNotNull(department);
        assertEquals(1, 1);
    }

    @Test
    void addDepartment() {
        Department department = new Department();
        department.setDepartId(1);
        department.setDepartName("HR");

        when(departmentRepository.save(department)).thenReturn(department);
        assertNotNull(department);
    }

    @Test
    void updateDepartment() {
        Department department = new Department();
        department.setDepartId(1);

        when(departmentRepository.findById(department.getDepartId())).thenReturn(java.util.Optional.of(department));
        department.setDepartName("IT");
        when(departmentRepository.save(department)).thenReturn(department);
        assertNotNull(department);
        assertEquals(1, 1);

    }

    @Test
    void deleteDepartmentById() {
        Department department = new Department();
        department.setDepartId(1);

        when(departmentRepository.findById(department.getDepartId())).thenReturn(java.util.Optional.of(department));
        department.setDepartName("");
        when(departmentRepository.save(department)).thenReturn(department);
        assertNotNull(department);
        assertEquals(1, 1);
    }
}