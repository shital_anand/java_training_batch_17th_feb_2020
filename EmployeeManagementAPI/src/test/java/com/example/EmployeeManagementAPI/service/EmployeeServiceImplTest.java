package com.example.EmployeeManagementAPI.service;

import com.example.EmployeeManagementAPI.dto.Department;
import com.example.EmployeeManagementAPI.dto.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

class EmployeeServiceImplTest extends Object {

    @InjectMocks
    EmployeeServiceImpl employeeService;
    @Mock
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllEmployees() {
        Employee employee = new Employee();
        List<Employee> employeeList = new ArrayList<>();
        Employee employee1 = new Employee();
        employee.setEmpId(1);
        employee.setActive(true);
        employee.setEmail("shital.anand@unthinkable.co");
        employee.setEmpName("Shital Anand");

        employee1.setEmpId(2);
        employee1.setActive(true);
        employee1.setEmail("swarnalata@unthinkable.co");
        employee1.setEmpName("Swarnalata");

        employeeList.add(employee);
        employeeList.add(employee1);
        when(employeeRepository.findAll()).thenReturn(employeeList);
        assertNotNull(employee);
    }

    @Test
    void getEmployeeById() {
        Employee employee = new Employee();
        employee.setEmpId(1);
        when(employeeRepository.findById(employee.getEmpId())).thenReturn(java.util.Optional.of(employee));

        assertNotNull(employee);
        assertEquals(1, 1);
    }

    @Test
    void addEmployee() {
        Employee employee = new Employee();
        Department department = new Department();
        List<Department> departmentList = new ArrayList<>();
        department.setDepartId(1);
        employee.setEmpId(1);
        employee.setActive(true);
        employee.setEmail("shital.anand@unthinkable.co");
        employee.setEmpName("Shital Anand");
        departmentList.add(department);
        employee.setDepartment(departmentList);

        when(employeeRepository.save(employee)).thenReturn(employee);
        assertNotNull(employee);
    }

    @Test
    void addAnotherDepartment() {
        Employee employee = new Employee();
        Department department = new Department();
        List<Department> departmentList = new ArrayList<>();
        department.setDepartId(1);
        employee.setEmpId(1);
        departmentList.add(department);
        employee.setDepartment(departmentList);

        when(employeeRepository.findById(employee.getEmpId())).thenReturn(java.util.Optional.of(employee));
        when(employeeRepository.save(employee)).thenReturn(employee);
        assertNotNull(employee);
        assertEquals(1, 1);
    }

    @Test
    void deleteEmployee() {
        Employee employee = new Employee();
        employee.setEmpId(1);
        employee.setActive(false);

        when(employeeRepository.findById(employee.getEmpId())).thenReturn(java.util.Optional.of(employee));
        when(employeeRepository.save(employee)).thenReturn(employee);
        assertNotNull(employee);
        assertEquals(1, 1);

    }

    @Test
    void updateEmployee() {
        Employee employee = new Employee();
        employee.setEmpId(1);
        employee.setEmail("shital007.anand@unthinkable.co");

        when(employeeRepository.findById(employee.getEmpId())).thenReturn(java.util.Optional.of(employee));
        when(employeeRepository.save(employee)).thenReturn(employee);
        assertNotNull(employee);
        assertEquals(1, 1);
    }
}